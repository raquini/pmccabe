# Copyright (c) 2002 Hewlett-Packard under GPL version 2 or later
CFILES=\
	cparse.c      dmain.c	    gettoken.c	  nmain.c	pmccabe.c \
	getopt.c	    io.c

OFILES=$(CFILES:.c=.o)

CFLAGS = -Wall -Wextra -O3 -fwhole-program

INCLUDES=config.h dmain.h getopt.h pmccabe.h

###############{

# On HP-UX you will have to change this
INSTALL = install -o root -g root

DESTDIR = 

PROGS = codechanges pmccabe decomment vifn

MANPGS = codechanges.1 pmccabe.1 vifn.1 decomment.1

DOCS = TODO NEWS COPYING

PMOBJS	      = combined.o

all:		$(PROGS)

test:		$(PROGS)
		cd tests && ./testsuite

pmccabe:	$(PMOBJS)
	$(CC) $(CFLAGS) -o pmccabe $(PMOBJS)

combined.o:	$(CFILES)
	$(CC) $(CFLAGS) -c combined.c

clean:
	rm -f *.[oa] pmccabe decomment *.out */*.out

install: $(PROGS) $(MANPGS) $(DOCS)
	$(INSTALL) -d $(DESTDIR)/usr/share/doc/pmccabe \
			$(DESTDIR)/usr/share/man/man1 \
			$(DESTDIR)/usr/bin
	$(INSTALL) -m 644 $(MANPGS) $(DESTDIR)/usr/share/man/man1
	#$(INSTALL) -m 644 $(DOCS)  $(DESTDIR)/usr/share/doc/pmccabe
	$(INSTALL) -m 755 $(PROGS) $(DESTDIR)/usr/bin

indent:
	indent -bap -bad -d0 -bli0 -nce -cli0 -ss -npcs -bs -di1 -nbc -psl -i4 -lp -ip0 -nfca -nfc1 *.c *.h

###############}

getopt.o :		config.h getopt.h

dmain.o \
io.o :		dmain.h pmccabe.h

cparse.o \
gettoken.o \
nmain.o :		pmccabe.h dmain.h

pmccabe.o :		pmccabe.h getopt.h
